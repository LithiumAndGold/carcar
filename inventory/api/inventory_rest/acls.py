# outside server testing
from .keys import PEXELS_API_KEY

import requests
import json


def get_photo(manufacturer, model):
    url = "https://api.pexels.com/v1/search"
    query = {"query": f"{manufacturer}, {model}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=query, headers=headers)
    content = response.json()
    try:
        picture_url = content["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}
