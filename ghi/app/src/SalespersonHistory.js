import React, { useEffect, useState } from "react";

function SalespersonHistory() {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [saleId, setSaleId] = useState("")

  const handleSaleIdChange = (event) => {
    const value = event.target.value;
    setSaleId(value)
  }

  async function loadSales() {
    const response = await fetch("http://localhost:8090/api/sales/")
    if (response.ok) {
      const data = await response.json();
      setSales(data);

    }
  }

  useEffect(() => {
    loadSales();
  }, [])

  async function loadSalespeople() {
    const response = await fetch("http://localhost:8090/api/salespeople/")
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data);
    }
  }

  useEffect(() => {
    loadSalespeople();
  }, [])

  let key = 1

  return (
    <div>
      <div>
        <h2>Select a salesperson</h2>
        <div>
          <select onChange={handleSaleIdChange} id="salesperson" name="salesperson" className="form-select mb-3">
            <option key="" value="">Select salesperson</option>
            {salespeople.salespeople?.map(sperson => {
              return(
                <option key={sperson.id} value={sperson.id}>{sperson.last_name}, {sperson.first_name}</option>
                );
            })}
          </select>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.sales?.map(sale => {
            if (sale.salesperson.id == saleId) {
            return (
              <tr key={ key ++ }>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
              </tr>
            )}
          })}
        </tbody>
      </table>
    </div>
  )
}

export default SalespersonHistory;
