import React, { useEffect, useState } from "react";

function AutomobilesList() {
  const [automobiles, setAutomobiles] = useState([]);

  async function loadAutomobiles() {
  const response = await fetch("http://localhost:8100/api/automobiles/")
  if (response.ok) {
    const data = await response.json();
    setAutomobiles(data.autos);
  }
  }

  useEffect(() => {
    loadAutomobiles();
  }, [])

  let key = 1

  const deleteAuto = async (event) => {
  const autoId = event.target.value

  const autoUrl = `http://localhost:8100/api/automobiles/${autoId}/`;
  const fetchConfig = {
    method: "DELETE",
  };

  const response = await fetch(autoUrl, fetchConfig);
    if (response.ok) {

      window.location.reload();
    }
  }

  return (
    <div>
      <h2>List of Automobiles</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Color</th>
            <th>Year</th>
            <th>VIN</th>
            <th>Model</th>
            <th>Sold</th>
            <th>Delete</th>

          </tr>
        </thead>
        <tbody>
          {automobiles.map(automobile => {
            return (
              <tr key={ key ++ }>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.vin}</td>
                <td>{automobile.model.manufacturer.name} {automobile.model.name}</td>
                <td>{automobile.sold.toString()}</td>
                <td><button onClick={deleteAuto} className="btn btn-danger" id="delete" value={automobile.vin}>Delete</button></td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default AutomobilesList;
