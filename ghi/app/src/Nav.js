import { NavLink, Link } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#nav">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-l1-0">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Inventory</a>
              <ul className="dropdown-menu">
                <li style={{ position: "relative" }}>
                  <NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink>
                  <Link to="/manufacturers/new"><img style={{ right: "15px", position: "absolute", top: "50%", transform: "translateY(-50%)" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi-oDcYjMIeuPpvHEanc5bAqKAZMBKNcwp56Xk1izvDknsReWBDmRH4ZtPM9v3Cb_qkOY&usqp=CAU" width="15" alt="New" /></Link>
                </li>
                <li style={{ position: "relative" }}>
                  <NavLink className="dropdown-item" to="/models">Models</NavLink>
                  <Link to="/models/new"><img style={{ right: "15px", position: "absolute", top: "50%", transform: "translateY(-50%)" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi-oDcYjMIeuPpvHEanc5bAqKAZMBKNcwp56Xk1izvDknsReWBDmRH4ZtPM9v3Cb_qkOY&usqp=CAU" width="15" alt="New" /></Link>
                </li>
                <li style={{ position: "relative" }}>
                  <NavLink className="dropdown-item" to="/automobiles">Automobiles</NavLink>
                  <Link to="/automobiles/new"><img style={{ right: "15px", position: "absolute", top: "50%", transform: "translateY(-50%)" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi-oDcYjMIeuPpvHEanc5bAqKAZMBKNcwp56Xk1izvDknsReWBDmRH4ZtPM9v3Cb_qkOY&usqp=CAU" width="15" alt="New" /></Link>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Sales</a>
              <ul className="dropdown-menu">
                <li style={{ position: "relative" }}>
                  <NavLink className="dropdown-item" to="/salespeople">Salespeople</NavLink>
                  <Link to="/salespeople/new"><img style={{ right: "15px", position: "absolute", top: "50%", transform: "translateY(-50%)" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi-oDcYjMIeuPpvHEanc5bAqKAZMBKNcwp56Xk1izvDknsReWBDmRH4ZtPM9v3Cb_qkOY&usqp=CAU" width="15" alt="New" /></Link>
                </li>
                <li style={{ position: "relative" }}>
                  <NavLink className="dropdown-item" to="/customers">Sales customers</NavLink>
                  <Link to="/customers/new"><img style={{ right: "15px", position: "absolute", top: "50%", transform: "translateY(-50%)" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi-oDcYjMIeuPpvHEanc5bAqKAZMBKNcwp56Xk1izvDknsReWBDmRH4ZtPM9v3Cb_qkOY&usqp=CAU" width="15" alt="New" /></Link>
                </li>
                <li style={{ position: "relative" }}>
                  <NavLink className="dropdown-item" to="/sales">Sales</NavLink>
                  <Link to="/sales/new"><img style={{ right: "15px", position: "absolute", top: "50%", transform: "translateY(-50%)" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi-oDcYjMIeuPpvHEanc5bAqKAZMBKNcwp56Xk1izvDknsReWBDmRH4ZtPM9v3Cb_qkOY&usqp=CAU" width="15" alt="New" /></Link>
                </li>
                <li><hr className="dropdown-divider"></hr></li>
                <li><NavLink className="dropdown-item" to="/salespeople/history">Salesperson History</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Service</a>
              <ul className="dropdown-menu">
                <li style={{ position: "relative" }}>
                  <NavLink className="dropdown-item" to="/technicians">Technicians</NavLink>
                  <Link to="/technicians/new"><img style={{ right: "15px", position: "absolute", top: "50%", transform: "translateY(-50%)" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi-oDcYjMIeuPpvHEanc5bAqKAZMBKNcwp56Xk1izvDknsReWBDmRH4ZtPM9v3Cb_qkOY&usqp=CAU" width="15" alt="New" /></Link>
                </li>
                <li style={{ position: "relative" }}>
                  <NavLink className="dropdown-item" to="/appointments">Appointments</NavLink>
                  <Link to="/appointments/new"><img style={{ right: "15px", position: "absolute", top: "50%", transform: "translateY(-50%)" }} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi-oDcYjMIeuPpvHEanc5bAqKAZMBKNcwp56Xk1izvDknsReWBDmRH4ZtPM9v3Cb_qkOY&usqp=CAU" width="15" alt="New" /></Link>
                </li>
                <li><hr className="dropdown-divider"></hr></li>
                <li><NavLink className="dropdown-item" to="/service/history">Service History</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
