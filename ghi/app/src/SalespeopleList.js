import React, { useEffect, useState } from "react";

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  async function loadSalespeople() {
    const response = await fetch("http://localhost:8090/api/salespeople/")
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data);
    }
  }

  useEffect(() => {
    loadSalespeople();
  }, [])

  const deleteSalesperson = async (event) => {
  const salespersonId = event.target.value

  const salespersonUrl = `http://localhost:8090/api/salespeople/${salespersonId}/`;
  const fetchConfig = {
    method: "DELETE",
  };

  const response = await fetch(salespersonUrl, fetchConfig);
    if (response.ok) {

      window.location.reload();
    }
  }

  return (
    <div>
      <h2>List of Salespeople</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Last name</th>
            <th>First name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.salespeople?.map(salesperson => {
            return (
              <tr key={salesperson.employee_id}>
                <td>{salesperson.last_name}</td>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.employee_id}</td>
                <td><button onClick={deleteSalesperson} className="btn btn-danger" id="delete" value={salesperson.id}>Delete</button></td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default SalespeopleList;
