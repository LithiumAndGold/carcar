import React, { useEffect, useState } from "react";

function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState([]);

    async function loadManufacturers() {
      const response = await fetch("http://localhost:8100/api/manufacturers/")
        if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers);
        }
      }

    useEffect(() => {
      loadManufacturers();
    }, [])

    let key = 1

    const deleteManufacturer = async (event) => {
      console.log('click')
      const manId = event.target.value
      console.log(event.target)

      const manUrl = `http://localhost:8100/api/manufacturers/${manId}/`;
      const fetchConfig = {
        method: "DELETE",
      };

      const response = await fetch(manUrl, fetchConfig);
      if (response.ok) {
        console.log('deleted')
        window.location.reload();
      }
    }

    return (
      <div className="row justify-content-center">
        <div className="col-auto w-50">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Manufacturer</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {manufacturers.map(manufacturer => {
                return (
                  <tr key={ key ++ }>
                    <td>{manufacturer.name}</td>
                    <td><button onClick={deleteManufacturer} className=
                      "btn btn-danger" id="delete" value={manufacturer.id}>Delete</button></td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    )
}

export default ManufacturersList;
