import React, { useEffect, useState } from "react";

function VehicleModelForm() {
  const [name, setName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [manufacturers, setManufacturers] = useState([]);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value)
  }
  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {}
    data["name"] = name;
    data["manufacturer_id"] = manufacturer;

    const modelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      const newModel = await response.json();
      window.location.reload(false)
      setName('');
      setManufacturer('');
    } else {
      console.error("Invalid url, please try another")
    }
  }
    const fetchData = async () => {
      const url = "http://localhost:8100/api/manufacturers/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers);
        }
      }

    useEffect(() => {
      fetchData();
    }, []);

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a model</h1>
            <form onSubmit={handleSubmit} id="create-model-form">
              <div>
                <select onChange={handleManufacturerChange} required id="manufacturer" name="manufacturer" className="form-select mb-3" value={manufacturer.id}>
                  <option key="null" value={manufacturer}>Manufacturer</option>
                  {manufacturers.map(manufacturer => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                      );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Model name" required type="text" name="name" id="name" className="form-control" value={name}/>
                <label htmlFor="name">Model name</label>
              </div>
               <div>
                <button className="btn btn-primary" type="submit">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
}

export default VehicleModelForm;
