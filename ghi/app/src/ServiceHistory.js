import React, { useState, useEffect } from "react";

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);
  const [search, setSearch] = useState("");
  const [vipFilter, setVipFilter] = useState("");
  const [vinSearch, setVinSearch] = useState("");
  const [customerSearch, setCustomerSearch] = useState("");
  const [dateSearch, setDateSearch] = useState("");
  const [timeSearch, setTimeSearch] = useState("");
  const [technicianSearch, setTechnicianSearch] = useState("");
  const [reasonSearch, setReasonSearch] = useState("");
  const [statusFilter, setStatusFilter] = useState("");
  const [pageSize, setPageSize] = useState(5);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    async function fetchAppointments() {
      try {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments);
        } else {
          console.error("Fetching appointments failed");
        }
      } catch (error) {
        console.error("Fetching appointments failed:", error);
      }
    }

    fetchAppointments();
  }, []);

  useEffect(() => {
    let filtered = appointments;

    if (vinSearch !== "") {filtered = filtered.filter(appointment =>
      appointment.vin.toLowerCase().startsWith(vinSearch.toLowerCase()));}

    if (vipFilter !== "") {filtered = filtered.filter(appointment =>
      appointment.vip === (vipFilter === "true"));}

    if (customerSearch !== "") {filtered = filtered.filter(appointment =>
        appointment.customer.toLowerCase().includes(customerSearch.toLowerCase()));}

    if (dateSearch !== "") {filtered = filtered.filter(appointment =>
        appointment.date.toLowerCase().includes(dateSearch.toLowerCase()));}

    if (timeSearch !== "") {filtered = filtered.filter(appointment =>
        appointment.time.toLowerCase().includes(timeSearch.toLowerCase()));}

    if (technicianSearch !== "") {filtered = filtered.filter(appointment =>
        `${appointment.technician.first_name} ${appointment.technician.last_name}`
          .toLowerCase().includes(technicianSearch.toLowerCase()));}

    if (reasonSearch !== "") {filtered = filtered.filter(appointment =>
        appointment.reason.toLowerCase().includes(reasonSearch.toLowerCase()));}

    if (statusFilter !== "") {filtered = filtered.filter(appointment =>
          appointment.status.toLowerCase() === statusFilter.toLowerCase());}

    setFilteredAppointments(filtered);
    setCurrentPage(1);
  }, [search, vinSearch, vipFilter, customerSearch, dateSearch, timeSearch, technicianSearch, reasonSearch, statusFilter, appointments]);

  const handlePageSizeChange = (event) => {
    setPageSize(parseInt(event.target.value));
    setCurrentPage(1);
  };

  const handlePageChange = (event) => {
    setCurrentPage(parseInt(event.target.value));
  };

  const pageCount = Math.ceil(filteredAppointments.length / pageSize);
  const paginatedAppointments = filteredAppointments.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );

  let key = 1

  return (
    <div>
      <h1>Service History</h1>
      <div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>VIP</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {paginatedAppointments.map((appointment) => (
              <tr key={ key ++ }>
                <td>{appointment.vin}</td>
                <td>{appointment.vip.toString().charAt(0).toUpperCase() + appointment.vip.toString().slice(1)}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date}</td>
                <td>{appointment.time}</td>
                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <th>
                <input type="text" placeholder="Search VIN" value={vinSearch} onChange={(e) => setVinSearch(e.target.value)} />
              </th>
              <th>
                <select value={vipFilter} onChange={(e) => setVipFilter(e.target.value)}>
                  <option value="">All</option>
                  <option value="true">True</option>
                  <option value="false">False</option>
                </select>
              </th>
              <th>
                <input type="text" placeholder="Search Customer" value={customerSearch} onChange={(e) => setCustomerSearch(e.target.value)} />
              </th>
              <th>
                <input type="text" placeholder="Search Date" value={dateSearch} onChange={(e) => setDateSearch(e.target.value)} />
              </th>
              <th>
                <input type="text" placeholder="Search Time" value={timeSearch} onChange={(e) => setTimeSearch(e.target.value)} />
              </th>
              <th>
                <input type="text" placeholder="Search Technician" value={technicianSearch} onChange={(e) => setTechnicianSearch(e.target.value)} />
              </th>
              <th>
                <input type="text" placeholder="Search Reason" value={reasonSearch} onChange={(e) => setReasonSearch(e.target.value)} />
              </th>
              <th>
                <select value={statusFilter} onChange={(e) => setStatusFilter(e.target.value)}>
                  <option value="">All</option>
                  <option value="created">Created</option>
                  <option value="finished">Finished</option>
                  <option value="canceled">Canceled</option>
                </select>
              </th>
            </tr>
          </tfoot>
        </table>
      </div>
      <div>
        <div>
          Show:
          <select value={pageSize} onChange={handlePageSizeChange}>
            <option value={5}>5</option>
            <option value={10}>10</option>
            <option value={25}>25</option>
          </select>
        </div>
        <div>
          Page:
          <select value={currentPage} onChange={handlePageChange}>
            {Array.from({ length: pageCount }, (_, i) => i + 1).map(
              (pageNum) => (
                <option key={pageNum} value={pageNum}>
                  {pageNum}
                </option>
              )
            )}
          </select>
        </div>
      </div>
    </div>
  );
}

export default ServiceHistory;
