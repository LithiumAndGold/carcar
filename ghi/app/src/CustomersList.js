import React, { useEffect, useState } from "react";

function CustomersList() {
  const [customers, setCustomers] = useState([]);

  async function loadCustomers() {
    const response = await fetch("http://localhost:8090/api/customers/")
    if (response.ok) {
      const data = await response.json();
      setCustomers(data);
    }
  }

  useEffect(() => {
    loadCustomers();
  }, [])

  const deleteCustomer = async (event) => {
  const customerId = event.target.value

  const customerUrl = `http://localhost:8090/api/customers/${customerId}/`;
  const fetchConfig = {
    method: "DELETE",
  };

  const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {

      window.location.reload();
    }
  }

  return (
    <div>
      <h2>List of Customers</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Address</th>
            <th>Phone number</th>
          </tr>
        </thead>
        <tbody>
          {customers.customers?.map(customer => {
            return (
              <tr key={customer.id}>
                <td>{customer.last_name}</td>
                <td>{customer.first_name}</td>
                <td>{customer.address}</td>
                <td>{customer.phone_number}</td>
                <td><button onClick={deleteCustomer} className="btn btn-danger" id="delete" value={customer.id}>Delete</button></td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default CustomersList;
