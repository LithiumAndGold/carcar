import React, { useEffect, useState } from "react";

function AutomobileForm() {
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [vin, setVin] = useState("");
    const [model, setModel] = useState("");
    const [models, setModels] = useState([]);

    const handleColorChange = (event) => {
      const value = event.target.value;
      setColor(value)
    }

    const handleYearChange = (event) => {
      const value = event.target.value;
      setYear(value)
    }

    const handleVinChange = (event) => {
      const value = event.target.value;
      setVin(value)
    }

    const handleModelChange = (event) => {
      const value = event.target.value;
      setModel(value)
    }

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {}
      data["color"] = color;
      data["year"] = year;
      data["vin"] = vin;
      data["model_id"] = model;

    const autoUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(autoUrl, fetchConfig);
      if (response.ok) {
        // window.location.reload(false)
        setColor("");
        setYear("");
        setVin("");
        setModel("");
        }
      }

    const fetchModels = async () => {
      const url = "http://localhost:8100/api/models/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        }
      }

    useEffect(() => {
      fetchModels();
    }, []);

    let key = 1

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an automobile</h1>
            <form onSubmit={handleSubmit} id="create-auto-form">
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color}/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleYearChange} placeholder="yyyy" required type="text" name="year" id="year" className="form-control" value={year}/>
                <label htmlFor="year">Year</label>
              </div>
               <div>
                <select onChange={handleModelChange} value={model} required id="model" name="model" className="form-select mb-3">
                  <option key="null" value="">Model</option>
                  {models.map(model => {
                    return (
                      <option key={ key ++ } value={model.id}>{model.manufacturer.name} {model.name}</option>
                      );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input maxLength={17} onChange={handleVinChange} placeholder="17-character-VIN" required type="text" name="vin" id="vin" className="form-control" value={vin}/>
                <label htmlFor="vin">17-character-VIN</label>
              </div>
               <div>
                <button className="btn btn-primary" type="submit">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
}

export default AutomobileForm;
