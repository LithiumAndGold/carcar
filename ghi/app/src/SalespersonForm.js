import React, { useState } from "react";

function SalepersonForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleFirstNameChange = (event) => {
      const value = event.target.value;
      setFirstName(value)
    }

    const handleLastNameChange = (event) => {
      const value = event.target.value;
      setLastName(value)
    }

    const handleEmployeeIdChange = (event) => {
      const value = event.target.value;
      setEmployeeId(value)
    }

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {}
      data["first_name"] = firstName;
      data["last_name"] = lastName;
      data["employee_id"] = employeeId;

      const salespersonUrl = "http://localhost:8090/api/salespeople/";
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };

      const response = await fetch(salespersonUrl, fetchConfig);
      if (response.ok) {

        setFirstName("");
        setLastName("");
        setEmployeeId("");
      }
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange} placeholder="First name" required type="text" name="firstName" id="firstName" className="form-control" value={firstName}/>
                <label htmlFor="firstName">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLastNameChange} placeholder="Last name" required type="text" name="lastName" id="lastName" className="form-control" value={lastName}/>
                <label htmlFor="lastName">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmployeeIdChange} placeholder="Employee Id #" required type="text" name="employeeId" id="employeeId" className="form-control" value={employeeId}/>
                <label htmlFor="employeeId">Employee Id #</label>
              </div>
              <div>
                <button className="btn btn-primary" type="submit">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
}

export default SalepersonForm;
