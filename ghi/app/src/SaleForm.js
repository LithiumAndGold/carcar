import React, { useEffect, useState } from "react";

function SaleForm() {
    const [price, setPrice] = useState("");
    const [salesperson, setSalesperson] = useState("");
    const [customer, setCustomer] = useState("");
    const [auto, setAuto] = useState("");
    const [salespeople, setSalespeople] = useState([]);
    const [autos, setAutos] = useState([]);
    const [customers, setCustomers] = useState([]);

    const handlePriceChange = (event) => {
      const value = event.target.value;
      setPrice(value)
    }

    const handleSalespersonChange = (event) => {
      const value = event.target.value;
      setSalesperson(value)
    }

    const handleCustomerChange = (event) => {
      const value = event.target.value;
      setCustomer(value)
    }

    const handleAutoChange = (event) => {
      const value = event.target.value;
      setAuto(value)
    }

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {}
      data["price"] = price;
      data["salesperson"] = salesperson;
      data["customer"] = customer;
      data["automobile"] = auto;

      const saleUrl = 'http://localhost:8090/api/sales/';
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {

          const autoUrl = `http://localhost:8100/api/automobiles/${auto}/`
          const autoFetchConfig = {
            method: "PUT",
            body: JSON.stringify({"sold": true}),
            headers: {
              'Content-Type': 'application/json',
            },
          };

          const autoResponse = await fetch(autoUrl, autoFetchConfig);
            if (!autoResponse.ok) {
              console.error("Could not update auto sold value")
            }

          setPrice('');
          setSalesperson('');
          setCustomer('');
          setAuto('');
        }
    }

    const fetchSalespeople = async () => {
      const url = "http://localhost:8090/api/salespeople/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setSalespeople(data.salespeople);
        }
      }

    useEffect(() => {
      fetchSalespeople();
    }, []);

    const fetchAutos = async () => {
      const url = "http://localhost:8100/api/automobiles/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setAutos(data.autos);
        }
      }

    useEffect(() => {
      fetchAutos();
    }, []);

    const fetchCustomers = async () => {
      const url = "http://localhost:8090/api/customers/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setCustomers(data.customers);
        }
      }

    useEffect(() => {
      fetchCustomers();
    }, []);

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
              <div className="form-floating mb-3">
                <input onChange={handlePriceChange} placeholder="Price" required type="number" name="price" id="price" className="form-control" value={price}/>
                <label htmlFor="price">Price</label>
              </div>
              <div>
                <select onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select mb-3" value={salesperson}>
                  <option value="">Select salesperson</option>
                  {salespeople.map(sperson => {
                    return (
                      <option key={sperson.id} value={sperson.id}>{sperson.last_name}, {sperson.first_name}</option>
                    )
                  })}
                </select>
              </div>
              <div>
                <select onChange={handleCustomerChange} required id="customer" name="customer" className="form-select mb-3" value={customer}>
                  <option value="">Select customer</option>
                  {customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.id}>{customer.last_name}, {customer.first_name}</option>
                    );
                  })}
                </select>
              </div>
              <div>
                <select onChange={handleAutoChange} required id="auto" name="auto" className="form-select mb-3" value={auto}>
                  <option value="">Select automobile</option>
                  {autos.map(autoVo => {
                    if (autoVo.sold === false) {
                      return (
                        <option key={autoVo.vin} value={autoVo.vin}>
                          {autoVo.year} {autoVo.model.manufacturer.name} {autoVo.model.name}, {autoVo.color}, VIN: {autoVo.vin}
                        </option>
                      )
                    }
                  })}
                </select>
              </div>
               <div>
                <button className="btn btn-primary" type="submit">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
}

export default SaleForm;
