import React, { useEffect, useState } from "react";
import { Trash } from "react-bootstrap-icons";

function VehicleModelsList() {
  const [models, setModels] = useState([]);

  async function loadModels() {
  const response = await fetch("http://localhost:8100/api/models/")
  if (response.ok) {
    const data = await response.json();
    setModels(data.models);
  }
  }

  useEffect(() => {
    loadModels();
  }, [])

  let key = 1

  const deleteModel = async (event) => {
    const modelId = event.target.value
    console.log(modelId)

    const modelUrl = `http://localhost:8100/api/models/${modelId}/`;
    const fetchConfig = {
      method: "DELETE",
    };

    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {

      window.location.reload();
    }
  }

  return (
    <div className="row justify-content-center">
      <div className="col-auto">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>Picture</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {models.map(model => {
              return (
                <tr key={ key ++ }>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                  <td><a href={model.picture_url}>{model.name}</a></td>
                  <td><button onClick={deleteModel} className="btn btn-danger" id="delete" value={model.id}>Delete</button></td>

                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default VehicleModelsList;
