import React, { useState } from "react";

function CustomerForm() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");

    const handleFirstNameChange = (event) => {
      const value = event.target.value;
      setFirstName(value)
    }

    const handleLastNameChange = (event) => {
      const value = event.target.value;
      setLastName(value)
    }

    const handleAddressChange = (event) => {
      const value = event.target.value;
      setAddress(value)
    }

    const handlePhoneNumberChange = (event) => {
      const value = event.target.value;
      setPhoneNumber(value)
    }

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {}
      data["first_name"] = firstName;
      data["last_name"] = lastName;
      data["address"] = address;
      data["phone_number"] = phoneNumber;

      const customerUrl = "http://localhost:8090/api/customers/";
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
          },
        };

      const response = await fetch(customerUrl, fetchConfig);
      if (response.ok) {
        const newCustomer = await response.json();

        setFirstName("");
        setLastName("");
        setAddress("");
        setPhoneNumber("");
      }
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange} placeholder="First name" required type="text" name="firstName" id="firstName" className="form-control" value={firstName}/>
                <label htmlFor="firstName">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLastNameChange} placeholder="Last name" required type="text" name="lastName" id="lastName" className="form-control" value={lastName}/>
                <label htmlFor="lastName">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" value={address}></textarea>
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePhoneNumberChange} placeholder="Phone number" required type="text" name="phoneNumber" id="phoneNumber" className="form-control" value={phoneNumber}/>
                <label htmlFor="phoneNumber">Phone number</label>
              </div>
              <div>
                <button className="btn btn-primary" type="submit">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
}

export default CustomerForm;
