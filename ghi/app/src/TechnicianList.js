import React, { useEffect, useState } from "react";


function TechnicianList(props) {
    const [technicians, setTechnicians] = useState([]);

    async function loadTechnicians() {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
        const data = await response.json();
        setTechnicians(data);
    }}

    useEffect(() => {
        loadTechnicians();
    }, []);

    if (!technicians) {
        return <div>No technicians available</div>
    }

    const deleteTech = async (event) => {
    const techId = event.target.value
    console.log(techId)

    const techUrl = `http://localhost:8080/api/technicians/${techId}/`;
    const fetchConfig = {
      method: "DELETE",
    };

    const response = await fetch(techUrl, fetchConfig);
      if (response.ok) {

        window.location.reload();
      }
    }

    return (
        <div>
            <h2>List of Technicians</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee Id</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.technicians?.map(technician => {
                    return (
                        <tr key={technicians.employee_id}>
                            <td>{ technician.first_name }</td>
                            <td>{ technician.last_name }</td>
                            <td>{ technician.employee_id }</td>
                            <td><button onClick={deleteTech} className="btn btn-danger" id="delete" value={technician.employee_id}>Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default TechnicianList;
