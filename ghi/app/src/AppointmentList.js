import React, { useEffect, useState } from "react";

function AppointmentList(props) {
    const [appointments, setAppointments] = useState([]);
    const [filteredAppointments, setFilteredAppointments] = useState([]);
    const [pageSize, setPageSize] = useState(5);
    const [currentPage, setCurrentPage] = useState(1);
    const [vinSearch, setVinSearch] = useState("");
    const [vipFilter, setVipFilter] = useState("");
    const [customerSearch, setCustomerSearch] = useState("");
    const [dateSearch, setDateSearch] = useState("");
    const [timeSearch, setTimeSearch] = useState("");
    const [technicianSearch, setTechnicianSearch] = useState("");
    const [reasonSearch, setReasonSearch] = useState("");

    async function loadAppointments() {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (response.ok) {
            const data = await response.json();
            const loadAppointments = data.appointments.filter(appointment =>
                appointment.status !== "Canceled" && appointment.status !== "Finished");

            const salesResponse = await fetch("http://localhost:8090/api/sales");
            if (salesResponse.ok) {
                const salesData = await salesResponse.json();
                const sales = salesData.sales;

                const updateAppointments = loadAppointments.map((appointment) => {
                    const matchSale = sales.find((sale) => sale.automobile.vin === appointment.vin);
                    if (matchSale) {
                        return {...appointment, vip: true};
                    }
                    return appointment;
                });
                setAppointments(updateAppointments);
                localStorage.setItem("appointments", JSON.stringify(updateAppointments));
            } else {
                console.error("Failed to fetch sales data");
            }
        } else {
            console.error("Failed to fetch appointments data");
        }
    };

    useEffect(() => {
        loadAppointments();
    }, []);

    useEffect(() => {
        let filtered = appointments;

        if (vinSearch !== "") {filtered = filtered.filter(appointment =>
            appointment.vin.toLowerCase().startsWith(vinSearch.toLowerCase()));}

        if (vipFilter !== "") {filtered = filtered.filter(appointment =>
            appointment.vip === (vipFilter === "true"));}

        if (customerSearch !== "") {filtered = filtered.filter(appointment =>
            appointment.customer.toLowerCase().includes(customerSearch.toLowerCase()));}

        if (dateSearch !== "") {filtered = filtered.filter(appointment =>
            appointment.date.toLowerCase().includes(dateSearch.toLowerCase()));}

        if (timeSearch !== "") {filtered = filtered.filter(appointment =>
            appointment.time.toLowerCase().includes(timeSearch.toLowerCase()));}

        if (technicianSearch !== "") {filtered = filtered.filter(appointment =>
            `${appointment.technician.first_name} ${appointment.technician.last_name}`
            .toLowerCase().includes(technicianSearch.toLowerCase()));}

        if (reasonSearch !== "") {filtered = filtered.filter(appointment =>
            appointment.reason.toLowerCase().includes(reasonSearch.toLowerCase()));}

        setFilteredAppointments(filtered);
        setCurrentPage(1);
    }, [appointments, vinSearch, vipFilter, customerSearch, dateSearch, timeSearch, technicianSearch, reasonSearch]);

    const handlePageSizeChange = (event) => {
        setPageSize(parseInt(event.target.value));
        setCurrentPage(1);
    };

    const handlePageChange = (event) => {
        setCurrentPage(parseInt(event.target.value));
    };

    const pageCount = Math.ceil(filteredAppointments.length / pageSize);
    const paginatedAppointments = filteredAppointments.slice(
        (currentPage - 1) * pageSize,
        currentPage * pageSize
    );

    async function cancelAppointment(appointmentId) {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/cancel`,
            {method: "PUT"});
            if (response.ok) {
                const updateAppointments = appointments.map(appointment => {
                    if (appointment.vin === appointmentId) {
                        return {...appointment, status: "canceled"};
                    }
                    return appointment;
                });
                const filterAppointments = updateAppointments.filter(appointment => appointment.status !== "canceled");
                setAppointments(filterAppointments);
                localStorage.setItem("appointments", JSON.stringify(filterAppointments));
            } else {
                console.error("Cancel appointment failed");
            }
        } catch (error) {
            console.error("Cancel appoitnment failed:", error);
        }
    }

    async function finishAppointment(appointmentId) {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/finish`,
            {method: "PUT"});
            if (response.ok) {
                const updateAppointments = appointments.map(appointment => {
                    if (appointment.vin === appointmentId) {
                        return {...appointment, status: "finished"};
                    }
                    return appointment;
                });
                const filterAppointments = updateAppointments.filter(appointment => appointment.status !== "finished");
                setAppointments(filterAppointments);
                localStorage.setItem("appointments", JSON.stringify(filterAppointments));
            } else {
                console.error("Finish appointment failed");
            }
        } catch (error) {
            console.error("Finish appoitnment failed:", error);
        }
    }

    let key = 1

    return (
        <div>
            <h2>List of appointments</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Automobile VIN</th>
                        <th>VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {paginatedAppointments.map(appointment => {
                        return (
                            <tr key={key++}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.vip.toString().charAt(0).toUpperCase() + appointment.vip.toString().slice(1)}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    <div className="btn-group">
                                        <button className="btn btn-primary" onClick={() => cancelAppointment(appointment.vin)}>Cancel</button>
                                        <button className="btn btn-primary" onClick={() => finishAppointment(appointment.vin)}>Finish</button>
                                    </div>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
                <tfoot>
            <tr>
              <th>
                <input type="text" placeholder="Search VIN" value={vinSearch} onChange={(e) => setVinSearch(e.target.value)} />
              </th>
              <th>
                <select value={vipFilter} onChange={(e) => setVipFilter(e.target.value)}>
                  <option value="">All</option>
                  <option value="true">True</option>
                  <option value="false">False</option>
                </select>
              </th>
              <th>
                <input type="text" placeholder="Search Customer" value={customerSearch} onChange={(e) => setCustomerSearch(e.target.value)} />
              </th>
              <th>
                <input type="text" placeholder="Search Date" value={dateSearch} onChange={(e) => setDateSearch(e.target.value)} />
              </th>
              <th>
                <input type="text" placeholder="Search Time" value={timeSearch} onChange={(e) => setTimeSearch(e.target.value)} />
              </th>
              <th>
                <input type="text" placeholder="Search Technician" value={technicianSearch} onChange={(e) => setTechnicianSearch(e.target.value)} />
              </th>
              <th>
                <input type="text" placeholder="Search Reason" value={reasonSearch} onChange={(e) => setReasonSearch(e.target.value)} />
              </th>
            </tr>
          </tfoot>
            </table>
            <div>
                <div>
                    Show:
                    <select value={pageSize} onChange={handlePageSizeChange}>
                        <option value={5}>5</option>
                        <option value={10}>10</option>
                        <option value={25}>25</option>
                    </select>
                </div>
                <div>
                    Page:
                    <select value={currentPage} onChange={handlePageChange}>
                        {Array.from({ length: pageCount }, (_, i) => i + 1).map((pageNum) => (
                            <option key={pageNum} value={pageNum}>
                                {pageNum}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
        </div>
    );
}

export default AppointmentList;
