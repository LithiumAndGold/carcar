import json
import requests
from datetime import time as datetime_time
from datetime import date
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Technician, Appointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin", "sold"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer",
        "date",
        "time",
        "reason",
        "status",
        "technician",
        "vip",
        "id"
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "technician": TechnicianListEncoder()
    }

    def default(self, obj):
        if isinstance(obj, datetime_time):
            return obj.strftime("%-I:%M %p")
        elif isinstance(obj, date):
            return obj.strftime("%m/%d/%Y")
        return super().default(obj)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            appointments = Appointment.objects.filter(
                automobile_id=automobile_vo_id
            )
        else:
            appointments = Appointment.objects.all()

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )

    else:
        try:
            content = json.loads(request.body)
            technician_id = content["technician"]
            technician = Technician.objects.get(employee_id=technician_id)
            vin = content["vin"]
            automobile_vo = AutomobileVO.objects.filter(vin=vin).first()
            vip_status = bool(automobile_vo)
            content["technician"] = technician
            content["status"] = "Created"
            content["vip"] = vip_status
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointments(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
                content_type="application/json"
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Invalid appointment ID"},
                                status=404)

    elif request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0}, status=200)
        except Appointment.DoesNotExist:
            message = {"message": "Invalid appointment ID"}
            return JsonResponse(message, status=400)

    else:
        content = json.loads(request.body)
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = content.get("status", appointment.status)
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
                status=200
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Invalid appointment ID"},
                                status=400)


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False
        )

    else:
        content = json.loads(request.body)
        try:
            technician = content.pop("technician", None)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_technicians(request, id):
    if request.method == "GET":
        try:
            technicians = Technician.objects.get(id=id)
            return JsonResponse(
                technicians,
                encoder=TechnicianListEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            message = {"message": "Invalid technician ID"}
            return JsonResponse(message, status=404)

    elif request.method == "PUT":
        content = json.loads(request.body)
        Technician.objects.filter(id=id).update(**content)
        technicians = Technician.objects.get(id=id)
        return JsonResponse(
            technicians,
            encoder=TechnicianListEncoder,
            safe=False,
            status=200
        )
    else:
        count, _ = Technician.objects.filter(employee_id=id).delete()
        return JsonResponse({"deleted": count > 0}, status=200)


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "Canceled"
        appointment.save()
        return JsonResponse(
            {"message": "Appointment canceled"}, status=200)
    except Appointment.DoesNotExist:
        message = {"message": "Invalid appointment ID"}
        return JsonResponse(message, status=400)


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "Finished"
        appointment.save()
        return JsonResponse(
            {"message": "Appointment finished"}, status=200)
    except Appointment.DoesNotExist:
        message = {"message": "Invalid appointment ID"}
        return JsonResponse(message, status=400)


@require_http_methods(["PUT"])
def api_update_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    content = json.loads(request.body)
    vip = content.get("vip", None)
    if vip is not None:
        appointment.vip = bool(vip)
    appointment.save()
