from django.db import models
from django.urls import reverse
from django.utils import timezone
from datetime import date


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    vin = models.CharField(max_length=200)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.IntegerField(
        unique=True,
        primary_key=True
        )

    def __str__(self):
        return str(self.employee_id)

    def get_technician_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

    class Meta:
        ordering = ("first_name", "last_name", "employee_id")


class Appointment(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateField(auto_now=False, default=date.today)
    time = models.TimeField(auto_now=False, default=timezone.now)
    reason = models.CharField(max_length=1000)
    status = models.CharField(max_length=200, null=True)
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"Appointment on {self.date}"

    def get_appointment_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})

    class Meta:
        ordering = (
            "vin",
            "vip",
            "customer",
            "date",
            "time",
            "technician",
            "reason",
            "status"
        )
